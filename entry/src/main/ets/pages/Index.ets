/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import router from '@ohos.router';
import { GlobalContext } from '@ohos/mars';


@Entry
@Component
struct Index {
  @State name: string = ""

  routeReport() {
    router.pushUrl({ url: 'pages/sample/statistic/ReportDisplayActivity'})
  }

  aboutToAppear(){
    let global = GlobalContext.getContext()
    global.setValue('taskHistory', [])
    global.setValue('sdtResults', [])
    global.setValue('wifiRecvFlow', Number)
    global.setValue('wifiSendFlow', Number)
    global.setValue('mobileRecvFlow', Number)
    global.setValue('mobileSendFlow', Number)
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Flex({ direction: FlexDirection.Row }) {

        Text('Mars Sample')
          .fontSize(50)
          .fontColor(Color.White)
          .width("90%")
          .fontWeight(FontWeight.Bold)

        Image($r("app.media.more_icon")).width("10%").height('8%').bindMenu([
          {
            value: 'show Statistics',
            action: () => {
              this.routeReport()
            }
          }
        ])
      }.backgroundColor(Color.Black)


      Button() {
        Text('STN Discuss')
          .fontSize(25)
          .width('100%')
          .height(40)
          .textAlign(TextAlign.Center)
      }
      .border({ width: 1 })
      .type(ButtonType.Normal)
      .width('100%')
      .height(100)
      .margin({
        top: 20
      })
      .backgroundColor('#0CFFFFFF')
      .onClick(() => {
        this.routeChat("0", "STN Discuss")
      })

      Button() {
        Text('Xlog Discuss')
          .fontSize(25)
          .width('100%')
          .height(40)
          .textAlign(TextAlign.Center)
      }
      .border({ width: 1 })
      .type(ButtonType.Normal)
      .width('100%')
      .height(100)
      .margin({
        top: 20
      })
      .backgroundColor('#0CFFFFFF')
      .onClick(() => {
        this.routeChat("1", "Xlog Discuss")
      })

      Button() {
        Text('SDT Discuss')
          .fontSize(25)
          .width('100%')
          .height(40)
          .textAlign(TextAlign.Center)
      }
      .border({ width: 1 })
      .type(ButtonType.Normal)
      .width('100%')
      .height(100)
      .margin({
        top: 20
      })
      .backgroundColor('#0CFFFFFF')
      .onClick(() => {
        this.routeChat("2", "SDT Discuss")
      })
    }
    .width('100%')
    .height('100%')
  }

  private routeChat(topic:string, notice:string){
    router.pushUrl({
      url: 'pages/sample/chat/ChatActivitySample',
      params: {
        topic: topic,
        notice: notice
      },
    });
  }
}