/*
 * Tencent is pleased to support the open source community by making Mars available.
 * Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
 *
 * Licensed under the MIT License (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Constants from '../utils/Constants';

export class TableCell{
  public static STRING: number = 0;
  public static IMAGE: number = 1;
  public value: string;
  public width: number;
  public height: number;
  public type: number;

  public constructor(width: number, height: number, type: number, value?: string) {
    if (value != null) {
      this.value = value;
    }
    this.width = width;
    this.height = height;
    this.type = type;
  }
}

class StatisticsAdapter {
  private table: InstanceType<typeof TableRow>[] ;

  public getCount(): number {
    return this.table.length;
  }

  public getItemId(position: number): number {
    return position;
  }

  public setLines(lines: number): number {
    return lines;
  }

  public setGravity(gravity: number): number {
    return gravity;
  }

  public setText(text: string): string {
    return text;
  }

  public setImageResource(resId: string): string {
    return resId;
  }


  TableRowView = class {
    public TableRowView(tableRow: InstanceType<typeof TableRow>) {
      for (let i: number = 0;i < tableRow.getSize(); i++) {
        let tableCell = tableRow.getCellValue(i);
        let width = tableCell.width;
        let height = tableCell.height;
        //let layoutParams: LinearLayout.LayoutParams = new LinearLayout.LayoutParams(tableCell.width, tableCell.height);
        //layoutParams.setMargins(0, 0, 1, 1);
        let textCell = new StatisticsAdapter();
        if (tableCell.type == TableCell.STRING) {
          textCell.setLines(1);
          textCell.setGravity(Constants.CENTER);
          textCell.setText(tableCell.value.toString());
        } else if (tableCell.type == TableCell.IMAGE) {
          textCell.setImageResource(tableCell.value);
        }
      }
    }
  }



}
export class  TableRow   {
  private cell: InstanceType<typeof TableCell>[];

  public constructor(cell: InstanceType<typeof TableCell>[]) {
    this.cell = cell;
  }

  public getSize(): number {
    return this.cell.length;
  }

  public getCellValue(index: number): InstanceType<typeof TableCell> {
    if (index >= this.cell.length) {
      return null;
    }
    return this.cell[index];
  }
}
export default StatisticsAdapter;