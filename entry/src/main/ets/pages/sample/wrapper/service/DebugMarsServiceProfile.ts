/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
 * Default profile for testing.
 * <p></p>
 * Created by zhaoyuan on 2016/11/16.
 */
import MarsServiceProfile from './MarsServiceProfile'
class DebugMarsServiceProfile extends MarsServiceProfile {

    public static readonly MAGIC: number = 0x0110;
    public static readonly PRODUCT_ID: number = 200;
    public static readonly LONG_LINK_HOST: string = "123.57.87.139";
    public static readonly LONG_LINK_PORTS: number[] = [8081];
    public static readonly SHORT_LINK_PORT: number = 8080;

    public magic(): number {
        return DebugMarsServiceProfile.MAGIC;
    }

    public productID(): number {
        return DebugMarsServiceProfile.PRODUCT_ID;
    }

    public longLinkHost(): string {
        return DebugMarsServiceProfile.LONG_LINK_HOST;
    }

    public longLinkPorts(): number[] {
        return DebugMarsServiceProfile.LONG_LINK_PORTS;
    }

    public shortLinkPort(): number {
        return DebugMarsServiceProfile.SHORT_LINK_PORT;
    }
}

export default DebugMarsServiceProfile;