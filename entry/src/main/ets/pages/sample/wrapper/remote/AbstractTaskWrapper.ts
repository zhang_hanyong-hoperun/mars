/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import  { MarsTaskProperty }  from './MarsTaskProperty';
/**
 * MarsTaskWrapper using nano protocol buffer encoding
 * <p></p>
 * Created by zhaoyuan on 16/2/29.
 */
 abstract class AbstractTaskWrapper {
  public prop: Map<String, any> = new Map<String, any>();
  public constructor() {

    this.setHttpRequest("123.57.87.139", "/mars/sendmessage");
    this.setShortChannelSupport(false);
    console.info("setShortChannelSupport to false");
    this.setLongChannelSupport(true);
    console.info("setLongChannelSupport to true");
    this.setCmdID(3);
  }

  public getProperties(): Map<String, any>{
    return this.prop;
  }

  public abstract onTaskEnd(errType: number, errCode: number);

  public setHttpRequest(host: String, path: String): AbstractTaskWrapper {
    this.prop.set(MarsTaskProperty.OPTIONS_HOST, (""==host ? null : host));
    this.prop.set(MarsTaskProperty.OPTIONS_CGI_PATH, path);

    return this;
  }

  public setShortChannelSupport(support: boolean): AbstractTaskWrapper {
    this.prop.set(MarsTaskProperty.OPTIONS_CHANNEL_SHORT_SUPPORT, support);
    return this;
  }

  public setLongChannelSupport(support: boolean): AbstractTaskWrapper {
    this.prop.set(MarsTaskProperty.OPTIONS_CHANNEL_LONG_SUPPORT, support);
    return this;
  }

  public setCmdID(cmdID: number): AbstractTaskWrapper {
    this.prop.set(MarsTaskProperty.OPTIONS_CMD_ID, cmdID);
    return this;
  }


  public abstract req2buf():ArrayBuffer;

  public abstract buf2resp(buf: ArrayBuffer): number;

  public toString(): String {
    return "AbsMarsTask: " + this.prop.toString();
  }
}
export default AbstractTaskWrapper;