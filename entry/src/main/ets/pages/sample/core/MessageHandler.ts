/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import BusinessHandler from './BusinessHandler';
import { ChatDataCore } from '../chat/ChatDataCore';
import MessagePush from '../chat/proto/MessagePush';
import Constants from '../utils/Constants';
import PushMessage from '../wrapper/remote/PushMessage';

class MessageHandler extends BusinessHandler {
  public static TAG: string = "Mars.Sample.MessageHandler";

  public async handleRecvMessage(pushMessage: PushMessage):Promise<boolean> {
    console.info("onPush cmdid:" + pushMessage.cmdId + ", buffer:" +this.Uint8ArrayToString(new Uint8Array(pushMessage.buffer)) );
    console.info("MessageHandler.handleRecvMessage msg:" + JSON.stringify(pushMessage));
    switch (pushMessage.cmdId) {
      case Constants.PUSHCMD:
      {
        try {
          let message: MessagePush = new MessagePush();
         await message.mergeFrom(pushMessage.buffer);
          console.info("enter  handleRecvMessage topic:"+message.getTopic() + ", from:" + message.getFrom() + ", text:"+message.getContent());
          let data = new Map<string, string>([
            ["msgfrom", message.getFrom()],
            ["msgcontent", message.getContent()],
            ["msgtopic", message.getTopic()]
          ]);
          ChatDataCore.getInstance().onReceive(Constants.PUSHACTION, data);
        } catch (error) {
          console.info("MessageHandler.handleRecvMessage error: " + error);
        }
      }

      return true;
      default:
        break;
    }

    return false;
  }
  public Uint8ArrayToString(array: Uint8Array ): string {
    let out: string = '';
    let i: number = 0;
    let len: number = 0;
    let c: number = -1;
    let char2: number = -1;
    let char3: number = -1;
    let tempAry: Uint8Array = array;
    len = tempAry.length;
    i = 0;
    while (i < len) {
      c = tempAry[i++];
      switch (c >> 4) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        // 0xxxxxxx
          out += String.fromCharCode(c);
          break;
        case 12:
        case 13:
        // 110x xxxx   10xx xxxx
          char2 = tempAry[i++];
          out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
          break;
        case 14:
        // 1110 xxxx  10xx xxxx  10xx xxxx
          char2 = tempAry[i++];
          char3 = tempAry[i++];
          out += String.fromCharCode(((c & 0x0F) << 12) |
            ((char2 & 0x3F) << 6) |
            ((char3 & 0x3F) << 0));
          break;
      }
    }
    return out;
  }
}

export default MessageHandler;