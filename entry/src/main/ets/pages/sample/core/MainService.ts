/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import PushMessageHandler from '../wrapper/remote/PushMessageHandler';
import PushMessage from '../wrapper/remote/PushMessage';
import BusinessHandler from './BusinessHandler';
import MessageHandler from './MessageHandler';
import StatisticHandler from './StatisticHandler';

export default class MainService implements PushMessageHandler {
  public static TAG: string = "Mars.Sample.MainService";

  private pushMessages: PushMessage[] = new Array();

  private handlers: BusinessHandler[] = [
      new MessageHandler(),
      new StatisticHandler()
  ];

  private static INS: MainService = new MainService();

  private constructor(){
  }

  public static getInstance():MainService{
    return MainService.INS;
  }

  public async process(message: PushMessage) {
    if (message != null) {
      for (let handler of this.handlers) {
        if (await handler.handleRecvMessage(message)) {
          break;
        }
      }
    }
  }
}
