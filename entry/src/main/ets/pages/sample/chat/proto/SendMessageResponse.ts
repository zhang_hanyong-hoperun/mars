/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { GlobalContext } from '@ohos/mars';
import MessageBuilder from './MessageBuilder'

/**
 * SendMessageRequest proto定义
 *
 * message SendMessageResponse {
 *    enum Error {
 *          ERR_OK = 0;
 *          ERR_SYS = -1;
 *    }
 *    required int32 err_code = 1;
 *    required string err_msg = 2;
 *    required string from = 3;
 *    required string text = 4;
 *    required string topic = 5;
 }
 */
export enum Error {
   ERR_OK,
   ERR_SYS
};
export default class SendMessageResponse extends MessageBuilder {
  private err_code: number;
  private err_msg: string;
  private from: string;
  private text: string;
  private topic: string;

  private static proto: string =
    "message SendMessageResponse {" +
      "  enum Error {" +
      "       ERR_OK = 0;" +
      "       ERR_SYS = -1;" +
      "  }" +
      "  required int32 err_code = 1;" +
      "  required string err_msg = 2;" +
      "  required string from = 3;" +
      "  required string text = 4;" +
      "  required string topic = 5;" +
      "}";

  public async mergeFrom(buffer: ArrayBuffer) {
    console.info("enter mergeFrom, buffer length:" + buffer.byteLength + ", buffer:" + new Uint8Array(buffer));
    let root= GlobalContext.getContext().getValue('response')
    // @ts-ignore
    let Response = root.build("SendMessageResponse");
    try {
      let decodeMsg: SendMessageResponse = Response.decode(buffer);
      console.info("decode result:" + JSON.stringify(decodeMsg));
      this.err_code = decodeMsg.err_code;
      this.err_msg = decodeMsg.err_msg;
      this.from = decodeMsg.from;
      this.text = decodeMsg.text;
      this.topic = decodeMsg.topic;
    } catch (e) {
      console.info("decode exception:" + e);
    }
  }

  public getErrCode(): number {
    return this.err_code;
  }

  public setErrCode(err_code: number): void {
    this.err_code = err_code;
  }

  public getErrMsg(): string {
    return this.err_msg;
  }

  public setErrMsg(err_msg: string): void {
    this.err_msg = err_msg;
  }

  public getFrom(): string {
    return this.from;
  }

  public setFrom(from: string): void {
    this.from = from;
  }

  public getText(): string {
    return this.text;
  }

  public setText(text: string): void {
    this.text = text;
  }

  public getTopic(): string {
    return this.topic;
  }

  public setTopic(topic: string): void {
    this.topic = topic;
  }
}