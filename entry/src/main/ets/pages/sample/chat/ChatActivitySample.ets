/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import MarsServiceProxy from '../wrapper/remote/MarsServiceProxy';
import prompt from '@system.prompt';
import { ChatDataCore } from './ChatDataCore'
import { TextMessageTask } from './TextMessageTask'
import MarsServiceNative from '../wrapper/service/MarsServiceNative'
import MarsServiceProfile from '../wrapper/service/MarsServiceProfile'
import { MarsServiceStub } from '../wrapper/service/MarsServiceStub'
import { Mars, AppLogic, StnLogic, SdtLogic, GlobalContext } from '@ohos/mars';
import { Observer } from './Observer'
import { Observable } from './Observable'
import { ChatMsgEntity } from './ChatMsgEntity'
import router from '@system.router';
import SampleApplication from '../SampleApplicaton';
import protobuf from '@ohos/protobufjs';

@Entry
@Component
export struct ChatActivitySample {
  @StorageLink('time') time: string = ''
  @StorageLink('messageContent') messageContent: string = ''
  dialogWidth: number = 100;
  @State sendMessageFlag: Visibility = Visibility.None;
  @State shownSendMessage: string = '';
  @State messageTimeArray: string[] = new Array();
  @State messageContentArray: string[] = new Array();
  @State private editTextContent: string = '';
  @State lists: Array<ChatMsgEntity> = new Array<ChatMsgEntity>();
  @State fromName: string = '';
  @State textValue: string = '';
  private topicName: string = '';
  private notice: string = '';
  private profile: MarsServiceProfile = MarsServiceNative.gFactory.createMarsServiceProfile();
  private stub: MarsServiceStub = new MarsServiceStub(this.profile);
  accountInfo: AppLogic.AccountInfo = new AppLogic.AccountInfo(1, "");
  dialogController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogExample({
      ctx: this,
      onConfirm: this.onConfirm,
      textValue: $textValue,
      inputValue: $fromName
    }),
    autoCancel: true,
    alignment: DialogAlignment.Bottom,
    offset: { dx: 0, dy: -20 },
    gridCount: 4,
    customStyle: false
  })

  onConfirm(ctx: ESObject) {
    GlobalContext.getContext().setValue('from_name', ctx.fromName)
    if (ctx.stub) {
      ctx.stub.onChangFrom()
      ctx.accountInfo = ctx.stub.getAccountInfo();
    }
  }

  private getTitle(): string {
    if (this.notice == null || this.notice.length == 0) {
      this.notice = "Mars Sample";
    }
    return this.notice;
  }

  build() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center
    }) {
      Text(this.getTitle())
        .fontSize(40)
        .width('100%')
        .height(80)
        .fontColor(Color.White)
        .backgroundColor(Color.Black)
        .textAlign(TextAlign.Center)

      Scroll() {
        Column() {
          ForEach(this.lists, (item: ChatMsgEntity) => {
            if (item.getMsgType()) {
              leftDialog({
                time: item.getDate(),
                name: item.getName(),
                message: item.getMessage(),
                flag: $sendMessageFlag
              })
            } else {
              Dialog({
                time: item.getDate(),
                name: item.getName(),
                message: item.getMessage(),
                flag: $sendMessageFlag
              })
            }
          })
        }
      }
      .scrollBar(BarState.On)
      .scrollBarColor(Color.Gray)
      .scrollBarWidth(30)
      .onScroll((xOffset: number, yOffset: number) => {
        console.info(xOffset + ' ' + yOffset)
      })
      .onScrollEdge(() => {
        console.info('To the edge')
      })
      .onScrollEnd(() => {
        console.info('Scroll Stop')
      })
      .height('79%')
      .width('100%')

      Stack({ alignContent: Alignment.Bottom }) {
        Row({ space: 20 }) {
          TextInput({ placeholder: '请输入...', text: this.editTextContent })
            .type(InputType.Normal)
            .placeholderColor(Color.Gray)
            .fontColor('#333333')
            .enterKeyType(EnterKeyType.Search)
            .caretColor(Color.White)
            .width('70%')
            .margin({ left: 10, bottom: 10 })
            .height(42)
            .padding(10)
            .fontSize(20)
            .borderRadius('12px')
            .borderColor('#c8bebe')
            .backgroundColor(Color.White)
            .onChange((content: string) => {
              console.log("onChange content: " + content);
              this.editTextContent = content;
            });

          Button($r('app.string.btn_send'), { type: ButtonType.Normal, stateEffect: true })
            .borderRadius(8)
            .backgroundColor(0xd6d7d7)
            .width('18%')
            .margin({ bottom: 10 })
            .fontColor(Color.Black)
            .onClick(() => {
              this.onClickBtnSend();
              this.sendMessageFlag = Visibility.Visible;
              prompt.showToast({
                message: '发送',
                duration: 5000,
                bottom: 1200
              });
            })
        }.width('100%').height(52)
      }.backgroundColor('#5F7482').width('100%').height(70).margin({ top: 5 })
    }
    .width('100%')
    .height('100%')
    .backgroundColor('#F0F0E0')
  }

  public addObserver(): Observer {
    let that = this;
    console.info("addObserver: " + that.topicName);
    return { async update(o: Observable) {
      that.lists = ChatDataCore.getInstance().getTopicDatas(that.topicName)
      console.info("on update lists len:" + that.lists.length + ", lists:" + JSON.stringify(that.lists));
    }
    }
  }

  private getFormatDataTime(dateTime: Date): string {
    let year: number = dateTime.getFullYear(); //当前年
    let month: number = dateTime.getMonth() + 1;
    let date: number = dateTime.getDate();
    let hours: number = dateTime.getHours();
    let minutes: number = dateTime.getMinutes();
    let seconds: number = dateTime.getSeconds();
    return year.toString() + '-' + month.toString() + '-' + date.toString() + ' '
      + hours.toString() + ':' + minutes.toString() + ':' + seconds.toString();
  }

  public onClickBtnSend(): boolean {
    const message = this.editTextContent;
    if (message.length <= 0 || message == '') {
      prompt.showDialog({
        title: '提示',
        message: '不能输入空消息',
        buttons: [
          {
            text: '确定',
            color: '#666666',
          },
        ],
      });
      this.editTextContent = '';
      return false;
    }

    console.info("onClickBtnSend msg:" + message + ", topic:" + this.topicName)

    let entity: ChatMsgEntity = new ChatMsgEntity();
    entity.setName(this.accountInfo.userName);
    entity.setDate(ChatDataCore.getDate());
    entity.setMessage(message);
    entity.setMsgType(false);
    ChatDataCore.getInstance().addData(this.topicName, entity);

    this.editTextContent = "";

    MarsServiceProxy.send(new TextMessageTask(this.topicName, this.fromName, message)
      .onOK((onOk: ESObject) => {
        console.info('send------------ok');
        console.info("zhy MarsServiceProxy.send(new TextMessageTask(this.topicName, message)" + JSON.stringify(onOk));
      }).onError((onError: ESObject) => {
        console.error("zhy MarsServiceProxy.send(new TextMessageTask(this.topicName, message)" + JSON.stringify(onError));
        console.info('send------------error')
      })
    );
    return true;
  }

  public cancel(taskID: number): void {
    let TAG: string = "Mars.Sample.MarsServiceStub";
    console.error(`cancel 001` + taskID)
    //    StnLogic.stopTask(taskID);
  }

  private requestProto: string =
    "message SendMessageRequest {" +
      "required string access_token = 1;" +
      "required string from = 2;" +
      "required string to = 3;" +
      "required string text = 4;" +
      "required string topic = 5;" +
      "}";
  private responseProto: string =
    "message SendMessageResponse {" +
      "  enum Error {" +
      "       ERR_OK = 0;" +
      "       ERR_SYS = -1;" +
      "  }" +
      "  required int32 err_code = 1;" +
      "  required string err_msg = 2;" +
      "  required string from = 3;" +
      "  required string text = 4;" +
      "  required string topic = 5;" +
      "}";

  async aboutToAppear() {
    console.info(`aboutToAppear start`)
    let from: string = ''
    try {
      from = GlobalContext.getContext().getValue('from_name') as string
    } catch (e) {
      console.info('aboutToAppear----------err:' + e.message)
    }
    if (from == '') {
      this.dialogController.open()
    } else {
      this.fromName = from
    }
    let builder: ESObject = protobuf.newBuilder();
    let requestRoot: ESObject = await protobuf.loadProto(this.requestProto, builder, "chat.proto");
    let request: ESObject = await requestRoot.build("SendMessageRequest");
    let responseRoot: ESObject = await protobuf.loadProto(this.responseProto, builder, "chat.proto");
    GlobalContext.getContext().setValue('request', request)
    GlobalContext.getContext().setValue('response', responseRoot)

    this.topicName = router.getParams().topic.toString();
    this.notice = router.getParams().notice.toString();

    ChatDataCore.getInstance().addObserver(this.addObserver());

    // set callback
    AppLogic.setCallBack(this.stub);
    StnLogic.setCallBack(this.stub);
    SdtLogic.setCallBack(this.stub);

    SampleApplication.onCreate()
    // Initialize the Mars PlatformComm
    Mars.init();

    // Initialize the Mars
    StnLogic.setLonglinkSvrAddr(this.profile.longLinkHost(), this.profile.longLinkPorts());
    StnLogic.setShortlinkSvrAddr(this.profile.shortLinkPort());
    StnLogic.setClientVersion(this.profile.productID());
    Mars.onCreate(true);
    StnLogic.makesureLongLinkConnected();
    this.lists = ChatDataCore.getInstance().getTopicDatas(this.topicName)


    console.info(`aboutToAppear end`)
  }

  aboutToDisappear() {
    console.info(`zjj aboutToDisappear start`)
    Mars.onDestroy();
    ChatDataCore.getInstance().deleteObserver(this.addObserver());
    console.info(`zjj aboutToDisappear end`)
  }
}

@Component
struct Dialog {
  @Prop time: string
  @Prop name: string
  @Prop message: string
  @Link flag: Visibility
  dialogWidth: number = 100

  build() {
    Column() {
      Text(this.time)
        .fontSize(15)
        .width('40%')
        .height(30)
        .fontColor(Color.White)
        .textAlign(TextAlign.Center)
        .backgroundColor('#BFBFBF')
        .margin({ top: 5, bottom: 5 })
      Flex({ direction: FlexDirection.RowReverse, alignItems: ItemAlign.Center }) {
        Flex({ direction: FlexDirection.Column }) {
          Image($r('app.media.mini_avatar_shadow')).width(60).height(60)
          Text(this.name).fontSize(20)
          // .width(150).textOverflow({ overflow: TextOverflow.Ellipsis })
        }.margin({ right: 15 })

        Text(this.message)
          .textAlign(TextAlign.Center)
          .fontSize(20)
          .margin({ right: 10 })
          .padding({ left: 10, right: 10 })
          .maxLines(3)
          .constraintSize({ maxWidth: '70%', minHeight: 50, maxHeight: 160 })
          .backgroundColor(Color.White)
          .borderRadius(5)

      }.visibility(Visibility.Visible)
      .width('100%').height(90)

    }
  }
}

@Component
struct leftDialog {
  @Prop time: string
  @Prop name: string
  @Prop message: string
  @Link flag: Visibility
  dialogWidth: number = 100

  build() {
    Column() {
      Text(this.time)
        .fontSize(15)
        .width('50%')
        .height(30)
        .fontColor(Color.White)
        .textAlign(TextAlign.Center)
        .backgroundColor('#BFBFBF')
        .margin({ top: 5, bottom: 5 })

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
        Flex({ direction: FlexDirection.Column }) {
          Image($r('app.media.mini_avatar_shadow')).width(60).height(60)
          Text(this.name).fontSize(20)
          // .width(150).textOverflow({ overflow: TextOverflow.Ellipsis })
        }.margin({ left: 15 })

        Text(this.message)
          .fontSize(20)
          .maxLines(3)
          .constraintSize({ maxWidth: '70%', minHeight: 50, maxHeight: 160 })
          .margin({ left: 10, top: 20, right: 50 })
          .padding({ left: 10, right: 10 })
          .textAlign(TextAlign.Center)
          .backgroundColor(Color.White)
          .borderRadius(5)
        //       Image($r('app.media.chatto_bg_normal')).width()

      }.visibility(Visibility.Visible)
      .width('100%').height(90)
    }
  }
}

@CustomDialog
struct CustomDialogExample {
  @Link textValue: string
  @Link inputValue: string
  ctx: ESObject
  controller: CustomDialogController
  onConfirm: (ctx: ESObject) => void = (ctx: ESObject) => {
  }

  // 若尝试在CustomDialog中传入多个其他的Controller，以实现在CustomDialog中打开另一个或另一些CustomDialog，那么此处需要将指向自己的controller放在最后

  build() {
    Column() {
      Text('首次进入话题请输入昵称').fontSize(20).margin({ top: 10, bottom: 10 })
      TextInput({ placeholder: '', text: this.textValue }).height(60).width('90%')
        .onChange((value: string) => {
          this.textValue = value
        }).margin({ top: px2vp(20) })
      Flex({ justifyContent: FlexAlign.SpaceAround }) {

        Button('确认')
          .onClick(() => {
            if (this.textValue == '') {
              prompt.showToast({ message: "昵称不能为空" })
              return
            }
            this.inputValue = this.textValue
            this.onConfirm(this.ctx)
            this.controller.close()

          }).backgroundColor(0xffffff).fontColor(Color.Red)
      }.margin({ bottom: px2vp(20), top: px2vp(20) })
    }

    // dialog默认的borderRadius为24vp，如果需要使用border属性，请和borderRadius属性一起使用。
  }
}
