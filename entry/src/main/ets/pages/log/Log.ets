/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { Xlog } from '@ohos/mars';
import prompt from '@system.prompt';
import marsnapi from 'libmarsnapi.so';
import app from '@system.app';

const showToast =  (str: string)=> {
  console.log('showToast:' + str)
  prompt.showToast({
    message: str,
    duration: 2000,
  });
}

@Entry
@Component
struct Demo {
  scroller: Scroller= new Scroller()
  sendCount: number = 0;
  receiveCount: number = 0;
  randomKey: string = '';

  public showResult(result: number): void {
    if (0 >= result) {
      showToast("Sucess");
    } else {
      showToast("Error");
    }
  }

  aboutToAppear() {

  }

  aboutToDisappear() {

  }

  build() {
    Column({ space: 10 }) {
      Text('XLog Demo')
        .fontSize(30)
        .fontWeight(FontWeight.Bold)
      Scroll() {
        Column({ space: 5 }) {
          Button('创建单例newXlogInstance').onClick(event => {
            console.log("Xlog_newXlogInstance ");
                let info = app.getInfo();
                console.info("mars sampleXLog start data = " + getContext().filesDir);
                let logPath = getContext().filesDir + "/marsLog";
                let xLogConfig: Xlog.XLogConfig = new Xlog.XLogConfig(0, 0, logPath, "nameprefix", "LOGSAMPLE", 1, 1, logPath, 1,);
                let result:ESObject = marsnapi.Xlog_newXlogInstance(xLogConfig);
                this.showResult(result);
                console.log("Xlog_newXlogInstance result = " + result);
          })
          Button('获取单例getXlogInstance').onClick(event => {
            let result:ESObject = marsnapi.Xlog_getXlogInstance("nameprefix");
            this.showResult(result);
            console.log("Xlog_getXlogInstance  result = " + result);
          })
          Button('释放单例releaseXlogInstance').onClick(event => {
            console.log("releaseXlogInstance ");
            let result:ESObject = marsnapi.Xlog_releaseXlogInstance("nameprefix");
            this.showResult(result);
            console.log("Xlog_releaseXlogInstance  result = " + result);
          })
          Button('打开appenderOpen').onClick(event => {
            console.log("appenderOpen ");
            let logPath = getContext().filesDir + "/marsLog";
            console.log("Xlog_appenderOpen ");
            let result:ESObject = marsnapi.Xlog_appenderOpen(Xlog.LEVEL_DEBUG, Xlog.AppednerModeAsync, "", logPath, "LOGSAMPLE", 0);
            this.showResult(result);
            console.log("Xlog_appenderOpen  result = " + result);
          })
          Button('关闭appenderClose').onClick(event => {
            console.log("Xlog_appenderClose ");
            let result:ESObject = marsnapi.Xlog_appenderClose();
            this.showResult(result);
            console.log("Xlog_appenderClose  result = " + result);
          })
          Button('打开appenderFlush').onClick(event => {
            console.log("Xlog_appenderFlush ");
            let result:ESObject = marsnapi.Xlog_appenderFlush(0, true);
            this.showResult(result);
            console.log("Xlog_appenderFlush  result = " + result);
          })
          Button('测试logWrite').onClick(event => {
            console.log("Xlog_logWrite2 ");

            let logStr = "Mars xlog test";
            let result:ESObject = marsnapi.Xlog_logWrite2(0, 3,"tag", "filename", "funcname", 1, 100, 200, 120, logStr);
            this.showResult(result);
          })
          Button('获取getLogLevel').onClick(event => {
            console.log("Xlog_getLogLevel ");
            let result:ESObject = marsnapi.Xlog_getLogLevel();
            this.showResult(result);
            console.log("Xlog_getLogLevel  result = " + result);
          })
          Button('设置setLogLevel').onClick(event => {
            console.log("Xlog_setLogLevel ");
            let result:ESObject = marsnapi.Xlog_setLogLevel();
            this.showResult(result);
            console.log("Xlog_setLogLevel  result = " + result);
          })
          Button('测试setAppenderMode').onClick(event => {
            console.log("Xlog_setAppenderMode ");
            let result:ESObject = marsnapi.Xlog_setAppenderMode(1);
            this.showResult(result);
            console.log("Xlog_setAppenderMode  result = " + result);
          })
          Button('测试setConsoleLogOpen').onClick(event => {
            console.log("Xlog_setConsoleLogOpen ");
            let result:ESObject = marsnapi.Xlog_setConsoleLogOpen();
            this.showResult(result);
            console.log("Xlog_setConsoleLogOpen  result = " + result);
          })
          Button('测试setMaxFileSize').onClick(event => {
            console.log("Xlog_setMaxFileSize ");
            let result:ESObject = marsnapi.Xlog_setMaxFileSize(200);
            this.showResult(result);
            console.log("Xlog_setMaxFileSize  result = " + result);
          })
          Button('测试setMaxAliveTime').onClick(event => {
            console.log("Xlog_setMaxAliveTime ");
            let result:ESObject = marsnapi.Xlog_setMaxAliveTime(100);
            this.showResult(result);
            console.log("Xlog_setMaxAliveTime  result = " + result);
          })
        }.width('100%')
        .margin({ bottom: 150 })
      }
      .scrollable(ScrollDirection.Vertical).scrollBar(BarState.On)
      .scrollBarColor(Color.Gray).scrollBarWidth(30)
    }.gridSpan(10)
  }
}