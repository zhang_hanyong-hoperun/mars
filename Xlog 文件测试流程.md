# Xlog 文件测试流程

## 代码准备

 ![img](image/8.png)

## entry中config文件需将log置顶为启动页面

1. 应用内点击
- 创建单例
- 测试logwrite
- 释放单例

2. 观察控制台输出result=0即为成功写入

![img](image/9.png)

3. 打开终端,通过hdc命令确认是否有.Xlog文件生成

   ![img](image/10.png)

4. 先退出hdc后进行远程文件发送至本地操作

   ![img](image/11.png)

5. 查看本地桌面是否生成temp将temp文件粘贴到mars-master4\mars-master\mars\log\crypt目录下

6. 根据[官方文档](https://www.bookstack.cn/read/mars/86c33132ef2ede4c.md#Windows)需要安装:python 、openssl、setuptools、pip工具、[pyelliptic](https://github.com/yann2192/pyelliptic/releases/tag/1.5.7)。

    其中pip工具安装需进入C:\Python27\Scripts文件中终端执行 pip install pip==20.3.4，其他根据文档提示即可。

7. 根据[参考资料](https://blog.csdn.net/HanSnowqiang/article/details/121406872) 中配置对应私钥，公钥

8. 问题：Traceback (most recent call last):
    File "gen_key.py", line 3, in
    import pyelliptic
    File "build\bdist.win-amd64\egg\pyelliptic_*init*_.py", line 43, in
    File "build\bdist.win-amd64\egg\pyelliptic\openssl.py", line 527, in
    Exception: Couldn't load OpenSSL lib ...

    解决方法：https://github.com/tencent/mars/issues/286；试错经验，libeay32.dll文件解压后先试放入system32然后执行即可

![img](image/12.png)

9. 无效点击

   ![img](image/13.png)：

    解决方法：将命令拓展为python decode_mars_crypt_log_file.py ./temp.xLog D:\1.log；其中D:\1.log,为输出文件路径