# Mars

## 项目简介

Mars 是一个跨平台的网络组件，包括主要用于网络请求中的长连接，短连接，是基于 socket 层的解决方案，在网络调优方面有更好的可控性，暂不支持HTTP协议。
Mars 极大的方便了开发者的开发效率。

## 效果演示
![效果演示](preview.png)

## 编译运行

要编译通过mars需要在mars\library\src\main\cpp添加openssl依赖,需要自行编译[openssl集成到应用hap](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/openssl/docs/hap_integrate.md)

1.修改编译之前需要在交叉编译中支持编译x86_64架构，可以参考[adpater_architecture.md](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/docs/adpater_architecture.md)文档。

2.编译之前需要先修改HPKBUILD文件中openssl的版本

  ```
pkgver=OpenSSL_1_1_1t 
//修改为
pkgver=OpenSSL_1_1_1w
  ```

3.下载openssl的[OpenSSL_1_1_1w版本](https://github.com/openssl/openssl/archive/refs/tags/OpenSSL_1_1_1w.tar.gz)，执行以下命令获取对应的sha512值，替换SHA512SUM文件的内容。

  ```
sha512num openssl-OpenSSL_1_1_1w.tar.gz
  ```

4.在cpp目录下添加编译生成的openssl文件夹（会在lycium目录下生成usr文件夹里面会生成openssl文件夹）。

![img.png](img/img.png)

windows和mac请先合入patch，然后再编译此项目，参考以下两步：
1. 下载源码
````shell
git clone https://gitee.com/openharmony-sig/mars.git --recurse-submodules
````
2. 合入patch,顺序执行下面的命令即可自动合入patch。
````shell
chmod +x automatically_apply_mars_patch_files.sh

./automatically_apply_mars_patch_files.sh

````
至此patch合入完成。
## 下载安装

```shell
ohpm install @ohos/mars 
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明：

第一步： 初始化,导入 Mars 组件到自己项目中；
```mars
...
import { Mars, StnLogic, Xlog, BuildConfig } from '@ohos/Mars/';
import MarsServiceNative from '../wrapper/service/MarsServiceNative';
import MarsServiceProfile from '../wrapper/service/MarsServiceProfile';
import marsnapi from 'libmarsnapi.so';

...

```
第二步：方法调用，通过marsnapi调用对应方法；
```mars
marsnapi.BaseEvent_onCreate();
}
```

## 接口说明

* StnLogic.setLonglinkSvrAddr - 设置长链接的DEBUG IP
* StnLogic.StnLogic_setShortlinkSvrAddr - 设置短链接的DEBUG IP
* StnLogic.setDebugIP - 设置的DEBUG IP(不区分长/短链接)
* StnLogic.startTask - 任务启动
* StnLogic.stopTask - 任务停止
* StnLogic.hasTask - 判断任务是否存在
* StnLogic.redoTask - 重做所有长短连任务. 注意这个接口会重连长链接.
* StnLogic.clearTask - 停止并清除所有未完成任务.
* StnLogic.reset - 停止并清除所有未完成任务并重新初始化
* StnLogic.resetAndInitEncoderVersion - 停止并清除所有未完成任务并重新初始化, 重新设置encoder version
* StnLogic.setBackupIPs - 设置备份IP,用于long/short svr均不可用的场景下
* StnLogic.makesureLongLinkConnected - 检测长链接状态.如果没有连接上,则会尝试重连.
* StnLogic.setSignallingStrategy - 信令保活
* StnLogic.keepSignalling - 发送一个信令保活包
* StnLogic.stopSignalling - 停止信令保活
* StnLogic.setClientVersion - 设置客户端版本 放入长连私有协议头部
* StnLogic.getLoadLibraries - 获取底层已加载模块
* StnLogic.req2Buf - 网络层获取上层发送的数据内容
* StnLogic.buf2Resp - 网络层将收到的信令回包交给上层解析
* StnLogic.trafficData - 上报信令消耗的流量
* SdtLogic.setCallBack - 设置信令探测回调实例，探测结果将通过该实例通知上层
* SdtLogic.setHttpNetcheckCGI - 设置一个Http连通状态探测的URI
* Xlog.logWrite - 日志写入方式1
* Xlog.logWrite2 - 日志写入方式2
* Xlog.native_logWrite2 - 自定义日志写入
* Xlog.setLogLevel - 设置日志等级
* Xlog.getLogLevel - 获取日志等级
* Xlog.newXlogInstance - 创建xlog的日志单例
* Xlog.releaseXlogInstance - 释放xlog的日志单例

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本：4.1Canary(4.1.3.414) , OpenHarmony SDK：API 11 (4.1.0.55)
- DevEco Studio 版本：DevEco Studio NEXT Developer Preview2 ： 4.1.3.600 , OpenHarmony SDK: API11 (4.1.0)

## 目录结构
```
|---- library  
|     |---- cpp  # 示例代码文件夹
|           |---- mars # C++库文件
|     |---- ets
|           |---- sdt  
|                 |---- SdtLogic # sdt对外封装接口
|           |---- stn  
|                 |---- StnLogic # stn对外封装接口
|           |---- xlog # xlog对外封装接口
|                 |---- Xlog # xlog对外封装接口
|---- README.md  # 安装使用方法
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/mars/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/mars/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/openharmony-sig/mars/LICENSE) ，请自由地享受和参与开源。

