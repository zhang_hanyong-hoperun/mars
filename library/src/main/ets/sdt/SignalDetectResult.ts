/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 信令探测结果信息类
 */
class SignalDetectResult {

  public details: SignalDetectResult.ResultDetail;

  static ResultDetail = class {

    public detectType: number;

    public errorCode: number;

    public networkType: number;

    public detectIP: string;

    public connTime: number;

    public port: number;

    public rtt: number;

    public rttStr: string;

    public httpStatusCode: number;

    public pingCheckCount: number;

    public pingLossRate: string;

    public dnsDomain: string;

    public localDns: string;

    public dnsIP1: string;

    public dnsIP2: string;

    public toString(): string {
      return "ResultDetail{" +
        "detectType=" + this.detectType +
        ", errorCode=" + this.errorCode +
        ", networkType=" + this.networkType +
        ", detectIP='" + this.detectIP + '\'' +
        ", connTime=" + this.connTime +
        ", port=" + this.port +
        ", rtt=" + this.rtt +
        ", rttStr='" + this.rttStr + '\'' +
        ", httpStatusCode=" + this.httpStatusCode +
        ", pingCheckCount=" + this.pingCheckCount +
        ", pingLossRate='" + this.pingLossRate + '\'' +
        ", dnsDomain='" + this.dnsDomain + '\'' +
        ", localDns='" + this.localDns + '\'' +
        ", dnsIP1='" + this.dnsIP1 + '\'' +
        ", dnsIP2='" + this.dnsIP2 + '\'' +
        '}';
    }
  }

  public toString(): string {
    return "SignalDetectResult{" +
           "details=" + this.details.toString() +
           '}';
  }
}

namespace SignalDetectResult {
  export type ResultDetail = typeof SignalDetectResult.ResultDetail.prototype;
}

export default SignalDetectResult;