/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import netConnection from '@ohos.net.connection'
import marsnapi from 'libmarsnapi.so';
import wifi from '@ohos.wifi';

/**
 * 基础事件通知类
 */
class BaseEvent {
  public static onCreate(): number {
    console.info("Mars: BaseEvent_onCreate  ");
    var result = marsnapi.BaseEvent_onCreate();
    console.info("Mars: BaseEvent_onCreate result " + result);
    return result
  };

  public static onDestroy(): number {
    var result = marsnapi.BaseEvent_onDestroy();
    console.info("Mars: BaseEvent_onDestroy result " + result);
    return result
  };

  public static onNetworkChange(): number {
    var result = marsnapi.BaseEvent_onNetworkChange();
    console.info("Mars: onNetworkChange result " + result);
    return result
  };

  public static onForeground(forground: boolean): number{
    var result = marsnapi.BaseEvent_onForeground(forground);
    console.info("Mars: BaseEvent_onForeground result " + result);
    return result
  };

  public static onSingalCrash(sig: number): number{
    var result = marsnapi.BaseEvent_onSingalCrash(sig);
    console.info("Mars: BaseEvent_onSingalCrash result " + result);
    return result
  };

  public static onExceptionCrash(): number{
    var result = marsnapi.BaseEvent_onExceptionCrash();
    console.info("Mars: BaseEvent_onExceptionCrash result " + result);
    return result
  };

  /**
   * 网络切换监听，客户端通过注册该广播通知mars stn网络切换
   */


  static ConnectionReceiver = new class ConnectionReceiver {
    public lastActiveNetworkInfo: {
      netHandle: netConnection.NetHandle;
      netCap: netConnection.NetCapabilities;
    } = null;
    public lastWifiInfo: wifi.WifiLinkedInfo = null;
    public lastConnected: boolean = false;
    public TAG: string = "mars.ConnectionReceiver";

    constructor() {
      let NetConnection = netConnection.createNetConnection()
      NetConnection.on('netCapabilitiesChange', (data) => {
        this.checkConnInfo(data)
      })
      NetConnection.register((err) => {
      })
    }

    public checkConnInfo(data: {
      netHandle: netConnection.NetHandle;
      netCap: netConnection.NetCapabilities;
    }): void{
      if (data == null) {
        this.lastActiveNetworkInfo = null;
        this.lastWifiInfo == null
        BaseEvent.onNetworkChange();
      } else if (data.netCap.networkCap.indexOf(16) != -1) {
        if (this.lastConnected) {
          this.lastActiveNetworkInfo = null;
          this.lastWifiInfo = null;
          BaseEvent.onNetworkChange();
        }
        this.lastConnected = false;
      } else {
        this.isNetworkChange(data).then((value)=>{
          if (value) {
            BaseEvent.onNetworkChange();
          }
          this.lastConnected = true;
        })
      }

    }
    //获取wifi的赋值操作
    private async getWifiInfo() {
      await wifi.getLinkedInfo().then(wifiInfo => {
        if (wifiInfo != null && this.lastWifiInfo != null && wifiInfo.bssid != null && wifiInfo.ssid != null
        && this.lastWifiInfo.bssid == wifiInfo.bssid
        && this.lastWifiInfo.ssid == wifiInfo.ssid
        //            && this.lastWifiInfo.networkId == wifiInfo.networkId  网络配置ID， 仅系统应用可用。
        ) {
          return false;
        }
        this.lastWifiInfo = wifiInfo;
      }).catch(error => {
        this.lastWifiInfo = null
      });
    }

    public async isNetworkChange(data: {
      netHandle: netConnection.NetHandle;
      netCap: netConnection.NetCapabilities;
    }): Promise<boolean>{
      let isWifi = data.netCap.bearerTypes.indexOf(netConnection.NetBearType.BEARER_WIFI) != -1

      if (isWifi) {
       await this.getWifiInfo().then((value)=>{
        return value
       })
      } else if (this.lastActiveNetworkInfo != null
      && this.lastActiveNetworkInfo.netCap.networkCap.length <= 0 && data.netCap.bearerTypes.length <= 0
      && this.lastActiveNetworkInfo.netCap.bearerTypes == data.netCap.bearerTypes
      && this.lastActiveNetworkInfo.netCap.networkCap == data.netCap.networkCap) {
        return false;
      } else if (this.lastActiveNetworkInfo != null
      && this.lastActiveNetworkInfo.netCap.networkCap.length != 0 && data.netCap.bearerTypes.length != 0
      && this.lastActiveNetworkInfo.netCap.networkCap == data.netCap.networkCap
      ) {
        return false;
      }

      if (!isWifi) {
      }

      this.lastActiveNetworkInfo = data;
      return true;
    }
  }
}

export default BaseEvent;