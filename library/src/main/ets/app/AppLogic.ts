/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * APP的属性类
 */
class AppLogic {
  public static readonly TAG: string = "mars.AppLogic";

  /**
   * 帐号信息类
   */
  public static AccountInfo = class {
    /**
     * 帐号
     */
    public uin: number = 0;

    /**
     * 用户名
     */
    public userName: string = "";

    constructor(uin?: number, userName?: string) {
      this.uin = uin;
      this.userName = userName;
    }

  }

  /**
   * 终端设备信息类
   */
  public static DeviceInfo = class {

    /**
     * 设备名称
     */
    public devicename: string = "";

    /**
     * 设备类型
     */
    public devicetype: string = "";

    constructor(devicename: string, devicetype: string) {
      this.devicename = devicename;
      this.devicetype = devicetype;
    }
  }

  private static callBack: AppLogic.ICallBack = null;

  /**
   * 设置mars回调接口实例，mars回调上层时会调用该实例的方法
   * @param _callback
   */
  public static setCallBack(_callback: AppLogic.ICallBack): void {
    AppLogic.callBack = _callback;
  }

  /**
   * mars回调获取APP目录
   * @return
   */
  public static getAppFilePath(): string {
    try {
      if (AppLogic.callBack == null) {
        console.log("Mars: callback is null ");
        return null;
      }
      return AppLogic.callBack.getAppFilePath();
    } catch (e) {
      console.log('Failed to obtain the file directory. Cause: ' + e.message);
    }

    return null;

  }

  /**
   * mars回调获取用户帐号信息
   * @return
   */
  private static getAccountInfo(): AppLogic.AccountInfo {
    try {
      if (AppLogic.callBack == null) {

        console.log("Mars: callback is null ");
        return null;
      }
      return AppLogic.callBack.getAccountInfo();
    } catch (e) {
      console.log('Failed to obtain the file directory. Cause: ' + e.message);
    }
    return null;
  }

  /**
   * mars回调获取客户端版本号
   * @return
   */
  private static getClientVersion(): number {
    try {
      if (AppLogic.callBack == null) {
        console.log("Mars: callback is null ");
        return 0;
      }
      return AppLogic.callBack.getClientVersion();
    } catch (e) {
      console.log('Failed to obtain the file directory. Cause: ' + e.message);
    }

    return 0;
  }

  /**
   * mars回调获取终端设备信息
   * @return
   */
  private static getDeviceType(): AppLogic.DeviceInfo{
    try {
      if (AppLogic.callBack == null) {
        console.log("Mars: callback is null ");
        return null;
      }
      return AppLogic.callBack.getDeviceType();
    } catch (e) {
      console.log('Failed to obtain the file directory. Cause: ' + e.message);
    }

    return null;
  }
}

namespace AppLogic {

  export type AccountInfo = typeof AppLogic.AccountInfo.prototype;
  export type DeviceInfo = typeof AppLogic.DeviceInfo.prototype;

  /**
   * 关于APP信息的回调接口
   */
  export interface ICallBack {
    /**
      * STN 会将配置文件进行存储，如连网IPPort策略、心跳策略等，此类信息将会被存储在客户端上层指定的目录下
      * @return APP目录
      */
    getAppFilePath(): string;

    /**
     * STN 会根据客户端的登陆状态进行网络连接策略的动态调整，当用户非登陆态时，网络会将连接的频率降低
     * 所以需要获取用户的帐号信息，判断用户是否已登录
     * @return 用户帐号信息
     */
    getAccountInfo(): AppLogic.AccountInfo;

    /**
     * 客户端版本号能够帮助 STN 清晰区分存储的网络策略配置文件。
     * @return 客户端版本号
     */
    getClientVersion(): number;

    /**
     * 客户端通过获取设备类型，加入到不同的上报统计回调中，供客户端进行数据分析
     * @return
     */
    getDeviceType(): DeviceInfo;
  }
}

export default AppLogic;